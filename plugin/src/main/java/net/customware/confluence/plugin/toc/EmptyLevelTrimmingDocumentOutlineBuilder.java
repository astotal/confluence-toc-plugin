package net.customware.confluence.plugin.toc;

import java.util.ArrayList;
import java.util.List;

/**
 * A DocumentOutlineBuilder that will return an outline which doesn't contain empty levels (levels consisting only of placeholders).
 * This is useful in the context of an outline structure where heading levels have been completely ignored.
 */
public class EmptyLevelTrimmingDocumentOutlineBuilder extends DefaultDocumentOutlineBuilder
{
    @Override
    public DocumentOutline getDocumentOutline()
    {
        return beginPlaceholderTrimming(topOfOutline).getDocumentOutline();
    }

    private static DepthFirstDocumentOutlineBuilder beginPlaceholderTrimming(List<BuildableHeading> headings)
    {
        DepthFirstDocumentOutlineBuilder builder = new DefaultDocumentOutlineBuilder();

        // Create a temporary root node so that the same recursive method can be used for top level headings.
        BuildableHeading rootHeading = new BuildableHeading("__temporary_root_node__", null, 0);
        rootHeading.addChildren(headings);

        applyDiscardToHeading(builder, rootHeading, true);
        return builder;
    }

    /**
     * For the supplied heading check that it's children aren't all placeholders and if they are keep promoting descendants until there is a set of children that aren't all placeholders.
     * The modified heading is then added to the supplied builder at it's current level before the processes is repeated recursively for each of the children of the heading.
     *
     * @param builder the builder recording the outline structure
     * @param heading the current heading to be processed.
     * @param rootNode a flag that indicates whether the temporary root node is being processed (which shouldn't be added to the new builder)
     */
    private static void applyDiscardToHeading(DepthFirstDocumentOutlineBuilder builder, BuildableHeading heading, boolean rootNode)
    {
        BuildableHeading processed = discardPlaceholderChildren(heading);
        if (!rootNode)
        {
            builder.add(processed.getName(), processed.getAnchor(), processed.getType());
        }

        if (processed.hasChildren())
        {
            if (!rootNode)
                builder.nextLevel();

            for (BuildableHeading child : processed.getChildren())
            {
                applyDiscardToHeading(builder, child, false);
            }

            if (!rootNode)
                builder.previousLevel();
        }

    }

    /**
     * If all the children of the supplied heading are placeholders then promote all the descendants to direct children.
     *
     * @param heading the heading to process
     * @return the supplied heading with the children modified if necessary.
     */
    private static BuildableHeading discardPlaceholderChildren(BuildableHeading heading)
    {
        boolean allPlaceholderChildren = true;
        List<BuildableHeading> children = heading.getChildren();

        while (allPlaceholderChildren && !children.isEmpty())
        {
            List<BuildableHeading> candidateChildren = new ArrayList<BuildableHeading>();
            for (BuildableHeading child : children)
            {
                if (DocumentOutlineImpl.NOT_PLACEHOLDER_MATCHER.matches(child))
                {
                    allPlaceholderChildren = false;
                    break;
                }

                for (BuildableHeading grandChild : child.getChildren())
                    candidateChildren.add(grandChild);
            }

            if (allPlaceholderChildren)
            {
                children = candidateChildren;
                heading.clearChildren();
                heading.addChildren(children);
            }
        }

        return heading;
    }
}
