package net.customware.confluence.plugin.toc;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.confluence.macro.xhtml.RichTextMacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class MigrateAwayWikiCompatibleRegexParametersTest
{
    private MigrateAwayWikiCompatibleRegexParameters migrator;
    private MacroDefinition macroDefinition;
    private Map<String,String> macroParameters;
    
    @Mock
    private RichTextMacroMigration macroMigration;

    @Before
    public void setUp() throws Exception
    {
        migrator = new MigrateAwayWikiCompatibleRegexParameters(macroMigration);
        
        macroDefinition = new MacroDefinition();
        macroParameters = new HashMap<String,String>();
    }

    @Test
    public void testCommaInRegex()
    {
        macroDefinition.setName("toc-zone");
        macroParameters.put(AbstractTOCMacro.INCLUDE_PARAM, "a,b");
        macroParameters.put(AbstractTOCMacro.EXCLUDE_PARAM, "[a,b,c,d,e,f]");
        macroDefinition.setParameters(macroParameters);
        
        when(macroMigration.migrate(macroDefinition, null)).thenReturn(macroDefinition);

        MacroDefinition migrated = migrator.migrate(macroDefinition, null);
        assertEquals("a|b",migrated.getParameters().get(AbstractTOCMacro.INCLUDE_PARAM));
        assertEquals("[a|b|c|d|e|f]", migrated.getParameters().get(AbstractTOCMacro.EXCLUDE_PARAM));
    }

    @Test
    public void testEscapedCommaInRegex()
    {
        macroDefinition.setName("toc-zone");
        macroParameters.put(AbstractTOCMacro.INCLUDE_PARAM, "comma\\x2C");
        macroParameters.put(AbstractTOCMacro.EXCLUDE_PARAM, "another\\scomma\\s\\x2C");
        macroDefinition.setParameters(macroParameters);

        when(macroMigration.migrate(macroDefinition, null)).thenReturn(macroDefinition);
        
        MacroDefinition migrated = migrator.migrate(macroDefinition, null);
        assertEquals("comma,", migrated.getParameters().get(AbstractTOCMacro.INCLUDE_PARAM));
        assertEquals("another\\scomma\\s,", migrated.getParameters().get(AbstractTOCMacro.EXCLUDE_PARAM));
    }

    @Test
    public void testNoRegex()
    {
        macroDefinition.setName("toc-zone");
        macroParameters.put("apple", "strudel");
        macroDefinition.setParameters(macroParameters);

        when(macroMigration.migrate(macroDefinition, null)).thenReturn(macroDefinition);

        MacroDefinition migrated = migrator.migrate(macroDefinition, null);
        assertEquals(macroParameters, migrated.getParameters());
    }

    @Test
    public void testRegexRequiringNoModifications()
    {
        final String includeRegex = "monkey\\s+trousers";
        final String excludeRegex = "twisting|around";

        macroDefinition.setName("toc-zone");
        macroParameters.put(AbstractTOCMacro.INCLUDE_PARAM, includeRegex);
        macroParameters.put(AbstractTOCMacro.EXCLUDE_PARAM, excludeRegex);
        macroDefinition.setParameters(macroParameters);
        
        when(macroMigration.migrate(macroDefinition, null)).thenReturn(macroDefinition);

        MacroDefinition migrated = migrator.migrate(macroDefinition, null);
        assertEquals(includeRegex, migrated.getParameters().get(AbstractTOCMacro.INCLUDE_PARAM));
        assertEquals(excludeRegex, migrated.getParameters().get(AbstractTOCMacro.EXCLUDE_PARAM));
    }
}
