package it.net.customware.confluence.plugin.toc;

import com.atlassian.confluence.it.TestProperties;
import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;

import java.io.IOException;

/**
 * A wrapper class of AbstractConfluencePluginWebTestCase to make tests not to call `updateLicense` when running on OD mode
 */
public class AbstractCloudCompatibleTestCase extends AbstractConfluencePluginWebTestCase {

    @Override
    public void updateLicense() throws IOException {
        // do not call `updateLicense` when running on OD mode
        if (!TestProperties.isOnDemandMode()) {
            super.updateLicense();
        }
    }

    @Override
    public void restoreData() {
        // restores wipe out our nice user credentials... skip it cloud
        if (!TestProperties.isOnDemandMode()) {
            super.restoreData();
        }
    }
}
